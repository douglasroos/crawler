<?php namespace Crawler;

use Illuminate\Database\Eloquent\Model;

class Wishlist extends Model {
	
	public function user()
	{
		return $this->belongsTo('Crawler\User', 'user_id');
	}
	
	public function wishlistProducts()
	{
		return $this->hasMany('Crawler\WishlistProducts','wishlist_id');
	}

}
