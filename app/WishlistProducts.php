<?php

namespace Crawler;

use Illuminate\Database\Eloquent\Model;

class WishlistProducts extends Model
{
	
	protected $table = 'wishlist_products';
	
   	public function prods()
	{
		return $this->belongsTo('Crawler\Product','product_id');
	}
	
	public function wishlist()
	{
		return $this->belongsTo('Crawler\Wishlist','wishlist_id');
	}
}
