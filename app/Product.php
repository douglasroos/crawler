<?php namespace Crawler;

use Illuminate\Database\Eloquent\Model;

class Product extends Model {
	
	protected $table = 'products';

	public function wishlistProducts()
  	{
		return $this->hasMany('Crawler\WishlistProducts','product_id');
  	}

}
