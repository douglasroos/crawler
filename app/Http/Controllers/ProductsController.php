<?php namespace Crawler\Http\Controllers;

use Crawler\User;
use View;
use Curl;
use Cache;
use Crawler\Http\Requests;
use Crawler\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;

class ProductsController extends Controller {
	
	
	public $url;
	/**
	 * Function to list the products from the curl query
	 * @return \Illuminate\Http\Response
	 */
	public function show()
	{
		$productsTop   = $this->getProducts('desc');
		$productsCheap = array_reverse($this->getProducts());
		
		return View::make('products',array('productsTop' => $productsTop,'productsCheap' => $productsCheap));
	}
	
	/**
	 * Function to retrieve the products from the given URL's
	 * @return array
	 */
	private function getProducts($sort = '')
	{
		// First we check if the cache file exists for this query
		if (Cache::has('cache_products'.$sort)) {
			// If it exists we retur the cached array with the products
			$data = Cache::get('cache_products'.$sort);
		} else {
			// If not exists then we do our cURL call
			$this->url = 'https://www.appliancesdelivered.ie/search';
			$data = array();
			$limit = 10;
			switch($sort)
			{
				case 'desc':
					$response = Curl::to($this->url)
						->withData( array( 'sort' => 'price_desc' ) )
						->withOption('SSL_VERIFYPEER',false)
						->withOption('SSL_VERIFYHOST',  2)
						->withOption('ENCODING','gzip')
						->get();
					break;

				case 'asc':
					$response = Curl::to($this->url)
						->withOption('SSL_VERIFYPEER',false)
						->withOption('SSL_VERIFYHOST',  2)
						->withOption('ENCODING','')
						->get();
					break;

				default:
					$response = Curl::to($this->url)
						->withOption('SSL_VERIFYPEER',false)
						->withOption('SSL_VERIFYHOST',  2)
						->withOption('ENCODING','')
						->get();
					break;
			}

			$doc = new \archon810\SmartDOMDocument();
			$desc = array();
			@$doc->loadHTML($response);
			$xpath = new \DOMXPath($doc);
			$content = $xpath->query("//*[contains(@class, 'search-results')]");
			$pag = $doc->saveXML($content->item(0));
			$doc = new \archon810\SmartDOMDocument();
			@$doc->loadHTML($pag);
			$xpath = new \DOMXPath($doc);
			$searchResults = $xpath->query("//*[contains(@class, 'search-results-product row')]");
			if($searchResults->length < 10)
			{
				$limit = $searchResults->length;
			}
			for($i = 0; $i < $limit; $i++){
				$desc = $doc->saveXML($searchResults->item($i));
				$doc1 = new \archon810\SmartDOMDocument();
				@$doc1->loadHTML($desc);
				$xpath = new \DOMXpath($doc1);
				$rowsPic = $xpath->query("//*[contains(@class, 'product-image')]");
				$rowsDesc = $xpath->query("//*[contains(@class, 'product-description')]");
				$prodPic = $doc1->saveXML($rowsPic->item(0));
				$prodMDesc = $doc1->saveXML($rowsDesc->item(0));
				$doc1 = new \archon810\SmartDOMDocument();
				@$doc1->loadHTML($prodPic);
				$xpath = new \DOMXPath($doc1);
				$href = $xpath->query("//a/@href");
				$ahref = $doc1->saveXML($href->item(0));
				$doc1 = new \archon810\SmartDOMDocument();
				@$doc1->loadHTML($prodMDesc);
				$xpath = new \DOMXPath($doc1);
				$prodDesc = $xpath->query("//*[contains(@class, 'col-xs-12 col-sm-7 col-lg-8')]");
				$prodDesc = $doc1->saveXML($prodDesc->item(0));
				$doc1 = new \archon810\SmartDOMDocument();
				@$doc1->loadHTML($prodMDesc);
				$xpath = new \DOMXPath($doc1);
				$prodP = $xpath->query("//*[contains(@class, 'col-xs-12 col-sm-5 col-lg-4')]");
				$prodPr = $doc1->saveXML($prodP->item(0));
				$doc1 = new \archon810\SmartDOMDocument();
				@$doc1->loadHTML($prodPr);
				$xpath = new \DOMXPath($doc1);
				$prodPrice = $xpath->query("//*[contains(@class, 'section-title')]");
				$prodPrice = $doc1->saveXML($prodPrice->item(0));
				
				$url = str_replace('href="', "", $ahref);
				$url = str_replace('"', "", $url);

				$data[] = array("prodPic"=>$prodPic,"prodDesc"=>$prodDesc,'prodPrice' => $prodPrice, "url" => $url);
			}		
			
			Cache::put('cache_products'.$sort,$data, 60);
			
		}
		
		return $data;
	}
}
