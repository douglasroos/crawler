<?php namespace Crawler\Http\Controllers;

use Crawler\User;
use View;
use Crawler\Http\Requests;
use Crawler\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;

class UserController extends Controller {

	/**
	 * Show the form for editing the user profile.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit()
	{
		$id = Auth::user()->id;
		$userData = User::find($id);
		
		return View::make('user', array('user' => $userData));
	}

	/**
	 * Update the user profile on the database
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update()
	{
		$usr = User::find(Auth::user()->id);
		$updated = array();
		if(Input::has('email'))
		{
		  $usr->email = Input::get('email');
			$updated[] = 'E-mail';
		}
		if(Input::has('name'))
		{
		  $usr->name = Input::get('name');
		  $updated[] = 'Name';
		}

		$usr->save();
		
		return Redirect::to('profile')->with('info','Profile items updated: '.implode(",",$updated));
	}
	
	/**
	 * Show the form for editing the user password.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function passw()
	{	
		return View::make('passw');
	}
	
	/**
	 * Update the user password on the database
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function passwUpd()
	{		
		$rules = array(
		  'curp' => 'required',
		  'passw'  => 'required',
		  'passw2'  => 'required',
		);

		$messages = array(
		  'required' => 'The field :attribute it\'s mandatory.',
		);

		$validator = Validator::make(Input::all(), $rules, $messages);

		if ($validator->fails())
		{
		  return Redirect::to('profile/password')->withErrors($validator);
		}
		else
		{
		  $curpassw     = Input::get('curp');
		  $passwd       = Input::get('passw');
		  $passwd2      = Input::get('passw2');
		  $userPass 	= Auth::user()->password;
		  if(Hash::check($curpassw, $userPass))
		  {
			if(strlen($passwd) > 5 && strlen($passwd2) > 5)
			{
			  if($passwd == $passwd2)
			  {
				$update       	  = User::find(Auth::user()->id);
				$update->password = Hash::make($passwd);
				$update->save();

				return Redirect::to('profile/password')->with('info', 'Password updated successfully');
			  }
			  else
			  {
				return Redirect::to('profile/password')->with('error', 'Passwords doesn\'t match');
			  }
			}
			else
			{
			  return Redirect::to('profile/password')->with('error', 'Password too short');
			}
		  }
		  else
		  {
			return Redirect::to('profile/password')->with('error', 'Current password incorrect');
		  }
		}
	}

}
