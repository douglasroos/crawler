<?php namespace Crawler\Http\Controllers;

use Crawler\User;
use Crawler\Wishlist;
use Crawler\Product;
use Crawler\WishlistProducts;
use View;
use Crawler\Http\Requests;
use Crawler\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;

class WishlistController extends Controller {
	
	/**
	 * Function to add the product selected to the wishlist
	 * @return \Illuminate\Http\Response
	 */
	public function addTo()
	{
		$wishlist = Wishlist::where('user_id','=',Auth::user()->id)->first();
		if($wishlist)
		{
			$picture     	   = Input::get('prodPic');
		  	$description       = Input::get('prodDesc');
		  	$price      	   = Input::get('prodPrice');
			$url	      	   = Input::get('prodUrl');
			
			$prod = new Product();
			$prod->picture     = $picture;
			$prod->description = $description;
			$prod->price	   = $price;
			$prod->url		   = $url;
			$prod->save();
			$prodId = $prod->id;
			
			$wishlistprods = new WishlistProducts();
			$wishlistprods->wishlist_id = $wishlist->id;
			$wishlistprods->product_id	= $prodId;
			$wishlistprods->save();
			
			return Redirect::to('wishlist')->with('info', 'Product added to your wishlist');
			
		}
		else
		{
			$wishlist = new Wishlist();
			$wishlist->user_id = Auth::user()->id;
			$wishlist->save();
			$wishlistId = $wishlist->id;
			
			$picture     	   = Input::get('prodPic');
		  	$description       = Input::get('prodDesc');
		  	$price      	   = Input::get('prodPrice');
			$url	      	   = Input::get('prodUrl');
			
			$prod = new Product();
			$prod->picture     = $picture;
			$prod->description = $description;
			$prod->price	   = $price;
			$prod->url		   = $url;
			$prod->save();
			$prodId = $prod->id;
			
			$wishlistprods = new WishlistProducts();
			$wishlistprods->wishlist_id = $wishlistId;
			$wishlistprods->product_id	= $prodId;
			
			return Redirect::to('wishlist')->with('info', 'Product added to your wishlist');
		}
	}
	
	/**
	 * Function to show the products added to the wishlist
	 * @return \Illuminate\Http\Response
	 */
	public function show()
	{		
		$wishlist = Wishlist::with('wishlistProducts')->where('user_id','=',Auth::user()->id)->first();
		
		$products = WishlistProducts::with('prods')->where('wishlist_id','=',$wishlist->id)->get();
		
		return View::make("wishlists",array("products"=>$products));
	}
	
	/**
	 * Function to delete the product selected from the wishlist
	 * @return \Illuminate\Http\Response
	 */
	public function delete($id)
	{	
		if($prod = WishlistProducts::find($id)){
			$prod->delete();
			return Redirect::to('wishlist')->with('info', 'Product has been deleted from your wishlist');
		}else{
			return Redirect::to('wishlist')->with('error', 'The product doesn\'t exists on your wishlist');
		}
	}
}
