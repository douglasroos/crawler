<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index');

Route::get('wishlist','WishlistController@show');
Route::post('wishlist','WishlistController@addTo');
Route::get('wishlist/delete/{product}','WishlistController@delete');

Route::get('profile', 'UserController@edit');
Route::post('profile', 'UserController@update');
Route::get('profile/password', 'UserController@passw');
Route::post('profile/password', 'UserController@passwUpd');

Route::get('products', 'ProductsController@show');
