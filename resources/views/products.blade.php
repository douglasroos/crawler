@extends('adminlte::page')

@section('title', 'Products list')

@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">Product list</div>
				<div class="panel-body">
					<div class="col-sm-12">
						@if(count($productsTop) > 0 && count($productsCheap) > 0)
							
						@foreach($productsTop as $product)
						<div class="col-sm-12">
						{{ 
						  Form::open(array(
							'action' => array('WishlistController@addTo'),
							'method' => 'post'
						  )) 
						}}
							<input type="hidden" name="prodPic" value="<?php echo htmlentities($product['prodPic']); ?>">
							<input type="hidden" name="prodDesc" value="<?php echo htmlentities($product['prodDesc']); ?>">
							<input type="hidden" name="prodPrice" value="<?php echo htmlentities($product['prodPrice']); ?>">
							<input type="hidden" name="prodUrl" value="<?php echo $product['url']; ?>">
							<div class="col-sm-4"><?php echo $product['prodPic']; ?></div>
							<div class="col-sm-4"><?php echo $product['prodDesc']; ?></div>
							<div class="col-sm-2"><?php echo $product['prodPrice']; ?></div>
							<div class="col-sm-2"><button class="btn btn-primary" type="submit"><i class="fa fa-heart"></i> Add to wishlist</button></div>
						{{Form::close()}}
						</div>
						@endforeach

						@foreach($productsCheap as $product)
						<div class="col-sm-12">
						{{ 
						  Form::open(array(
							'action' => array('WishlistController@addTo'),
							'method' => 'post'
						  )) 
						}}
							<input type="hidden" name="prodPic" value="<?php echo htmlentities($product['prodPic']); ?>">
							<input type="hidden" name="prodDesc" value="<?php echo htmlentities($product['prodDesc']); ?>">
							<input type="hidden" name="prodPrice" value="<?php echo htmlentities($product['prodPrice']); ?>">
							<input type="hidden" name="prodUrl" value="<?php echo $product['url']; ?>">
							<div class="col-sm-4"><?php echo $product['prodPic']; ?></div>
							<div class="col-sm-4"><?php echo $product['prodDesc']; ?></div>
							<div class="col-sm-2"><?php echo $product['prodPrice']; ?></div>
							<div class="col-sm-2"><button class="btn btn-primary" type="submit"><i class="fa fa-heart"></i> Add to wishlist</button></div>
						{{Form::close()}}
						</div>
						@endforeach
						
						@else
						<div class="alert alert-danger">
							 <strong>There's no products to show you on this time.</strong>
						</div>
					@endif
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@stop
