@extends('adminlte::page')
@section('title', 'Change your password')

@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<div class="panel panel-default">
				<div class="panel-heading">Change your password</div>
				<div class="panel-body">
				@include('errors.errors')
					{{ 
					  Form::open(array(
						'action' => array('UserController@passwUpd'),
						'method' => 'post',
						'id'     => 'form-user-edit'
					  )) 
					}}
						<input type="hidden" name="_token" value="{{ csrf_token() }}">

						<div class="form-group">
							<label class="col-md-4 control-label">Current password</label>
							<div class="col-md-6">
								<input type="password" class="form-control" name="curp">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">New password</label>
							<div class="col-md-6">
								<input type="password" class="form-control" name="passw">
							</div>
						</div>
						
						<div class="form-group">
							<label class="col-md-4 control-label">Repeat password</label>
							<div class="col-md-6">
								<input type="password" class="form-control" name="passw2">
							</div>
						</div>

						<div class="form-group">
							<div class="col-md-6 col-md-offset-4">
								<button type="submit" class="btn btn-primary">
									Update
								</button>
							</div>
						</div>
					{{ Form::close() }}
				</div>
			</div>
		</div>
	</div>
</div>
@stop
