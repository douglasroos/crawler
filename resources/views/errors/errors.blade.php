@if(($info = Session::get('info', NULL)) !== NULL)
<div class="alert alert-success">{{ $info }}</div>
@endif

@if(($err = Session::get('error', NULL)) !== NULL)
<div class="alert alert-danger">{{ $err }}</div>
@endif

@if(count($errors) > 0)
<div class="alert alert-danger">
<ul>
@foreach($errors->all() as $error)
<li>{{ $error }}</li>
@endforeach
</ul>
</div>
@endif