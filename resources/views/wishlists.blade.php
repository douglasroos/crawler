@extends('adminlte::page')

@section('title', 'Your Wishlist')

@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">Your Wishlist</div>
				<div class="panel-body">
					@include('errors.errors')
					<div class="col-sm-12">
						@if(count($products) > 0)
 
						@foreach($products as $product)
						
						<div class="col-sm-12">
							<div class="col-sm-4"><?php echo $product['prods']->picture; ?></div>
							<div class="col-sm-4"><?php echo $product['prods']->description; ?></div>
							<div class="col-sm-2"><?php echo $product['prods']->price; ?></div>
							<div class="col-sm-2"><a href="{{ url( 'wishlist/delete/'.$product->id ) }}" class="btn btn-primary"><i class="fa fa-times"></i> Remove</a></div>
						</div>
						@endforeach
						
						@else
						<div class="alert alert-danger alert-dismissable">
						  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
							 <strong>You have no products on your wishlist.</strong>
						</div>
						@endif
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@stop
