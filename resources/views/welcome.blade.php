<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="">
	<meta name="author" content="">

	<title>Crawler made with Laravel</title>

	<!-- css -->
</head>
{!!Html::style('vendor/valera/css/bootstrap.min.css')!!} {!!Html::style('vendor/valera/font-awesome/css/font-awesome.min.css')!!} {!!Html::style('vendor/valera/css/nivo-lightbox.css')!!} {!!Html::style('vendor/valera/css/nivo-lightbox-theme/default/default.css')!!} {!!Html::style('vendor/valera/css/owl.carousel.css')!!} {!!Html::style('vendor/valera/css/owl.theme.css')!!} {!!Html::style('vendor/valera/css/flexslider.css')!!} {!!Html::style('vendor/valera/css/animate.css')!!} {!!Html::style('vendor/valera/css/style.css')!!} {!!Html::style('vendor/valera/color/default.css')!!}



<body id="page-top" data-spy="scroll" data-target=".navbar-custom">

	<!-- page loader -->
	<div id="page-loader">
		<div class="loader">
			<div class="spinner">
				<div class="spinner-container con1">
					<div class="circle1"></div>
					<div class="circle2"></div>
					<div class="circle3"></div>
					<div class="circle4"></div>
				</div>
				<div class="spinner-container con2">
					<div class="circle1"></div>
					<div class="circle2"></div>
					<div class="circle3"></div>
					<div class="circle4"></div>
				</div>
				<div class="spinner-container con3">
					<div class="circle1"></div>
					<div class="circle2"></div>
					<div class="circle3"></div>
					<div class="circle4"></div>
				</div>
			</div>
		</div>
	</div>
	<!-- /page loader -->

	<!-- Section: home video -->
	<!-- Navigation -->
	<div id="navigation">
		<nav class="navbar navbar-custom" role="navigation">
			<div class="container">
				<div class="row">
					<div class="col-md-2 mob-logo">
						<div class="row">
							<div class="site-logo">
								<a href="{{ url('/') }}"><img src="{{url('vendor/valera/img/logo-dark.png')}}" alt="" /></a>
							</div>
						</div>
					</div>


					<div class="col-md-10 mob-menu">
						<div class="row">
							<!-- Brand and toggle get grouped for better mobile display -->
							<div class="navbar-header">
								<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#menu">
                                                <i class="fa fa-bars"></i>
                                                </button>
							


							</div>
							<!-- Collect the nav links, forms, and other content for toggling -->
							<div class="collapse navbar-collapse" id="menu">
								<ul class="nav navbar-nav navbar-right">
									<li class="active"><a href="{{ url('/') }}">Home</a>
									</li>
									<li><a href="#about">About Us</a>
									</li>

									<li><a href="{{ url('/login') }}">Login</a>
									</li>
									<li><a href="{{ url('/register') }}">Register</a>
									</li>
									<li><a href="#contact">Contact</a>
									</li>

								</ul>
							</div>
							<!-- /.Navbar-collapse -->
						</div>
					</div>
				</div>
			</div>
			<!-- /.container -->
		</nav>
	</div>
	<!-- /Navigation -->
	<section id="intro" class="homepage-hero-module">

		<div class="video-container">
			<video autoplay loop class="fillWidth">
				<source src="{{url('vendor/valera/vids/home.mp4')}}" type="video/mp4"/>Your browser does not support the video tag. I suggest you upgrade your browser.
			</video>
			<div class="poster hidden">
				<img src="{{url('vendor/valera/img/home.jpg')}}" alt="Home Image">
			</div>
			<div class="overlay">
				<div class="text-center video-caption">
					<div class="wow bounceInDown" data-wow-offset="0" data-wow-delay="0.8s">
						<h1 class="big-heading font-light"><span id="js-rotating">Laravel Crawler, Get products from other webs, Add them to your wishlist, Simple and easy </span></h1>
					</div>
					<div class="wow bounceInUp" data-wow-offset="0" data-wow-delay="1s">
						<div class="margintop-30">
							<a href="{{ url('/login') }}" class="btn btn-skin" id="btn-scroll">Login to your account</a>
							<a href="{{ url('/register') }}" class="btn btn-skin" id="btn-scroll">Register as customer</a>
						</div>
					</div>
					<div class="col-sm-12">
						<a href="#about" class="scroll-about"><i class="fa fa-chevron-down circle-fa"></i></a>
					</div>
				</div>
			</div>
		</div>


	</section>
	<!-- /Section: intro -->

	<!-- Section: about -->
	<section id="about" class="home-section color-dark bg-white">
		<div class="container marginbot-50">
			<div class="row">
				<div class="col-lg-8 col-lg-offset-2">
					<div class="wow flipInY" data-wow-offset="0" data-wow-delay="0.4s">
						<div class="section-heading text-center">
							<h2 class="h-bold">About</h2>
							<div class="divider-header"></div>
							<p>Lorem ipsum dolor sit amet, agam perfecto sensibus usu at duo ut iriure.</p>
						</div>
					</div>
				</div>
			</div>

		</div>

		<div class="text-center">
			<div class="container">


				<div class="row">
					<div class="col-xs-6 col-sm-3 col-md-3">
						<div class="team-wrapper-big wow bounceInUp" data-wow-delay="0.2s">
							<div class="team-wrapper-overlay">
								<h5>Keyla Cruse</h5>
								<p>Graeco voluptua sed ea, malorum quaeque cotidieque per eu, quo id possit dissentias.</p>
								<div class="social-icons">
									<ul class="team-social">
										<li class="social-facebook"><a href="#"><i class="fa fa-facebook"></i></a>
										</li>
										<li class="social-twitter"><a href="#"><i class="fa fa-twitter"></i></a>
										</li>
										<li class="social-google"><a href="#"><i class="fa fa-google-plus"></i></a>
										</li>
									</ul>
								</div>
							</div>
							<img src="{{url('vendor/valera/img/team/1.jpg')}}" alt=""/>
						</div>
					</div>

					<div class="col-xs-6 col-sm-3 col-md-3">
						<div class="team-wrapper-big wow bounceInUp" data-wow-delay="0.5s">
							<div class="team-wrapper-overlay">
								<h5>Tanya Doe</h5>
								<p>Graeco voluptua sed ea, malorum quaeque cotidieque per eu, quo id possit dissentias.</p>
								<div class="social-icons">
									<ul class="team-social">
										<li class="social-facebook"><a href="#"><i class="fa fa-facebook"></i></a>
										</li>
										<li class="social-twitter"><a href="#"><i class="fa fa-twitter"></i></a>
										</li>
										<li class="social-google"><a href="#"><i class="fa fa-google-plus"></i></a>
										</li>
									</ul>
								</div>
							</div>
							<img src="{{url('vendor/valera/img/team/2.jpg')}}" alt=""/>
						</div>
					</div>
					<div class="col-xs-6 col-sm-3 col-md-3">
						<div class="team-wrapper-big wow bounceInUp" data-wow-delay="0.8s">
							<div class="team-wrapper-overlay">
								<h5>Heck Steven</h5>
								<p>Graeco voluptua sed ea, malorum quaeque cotidieque per eu, quo id possit dissentias.</p>
								<div class="social-icons">
									<ul class="team-social">
										<li class="social-facebook"><a href="#"><i class="fa fa-facebook"></i></a>
										</li>
										<li class="social-twitter"><a href="#"><i class="fa fa-twitter"></i></a>
										</li>
										<li class="social-google"><a href="#"><i class="fa fa-google-plus"></i></a>
										</li>
									</ul>
								</div>
							</div>
							<img src="{{url('vendor/valera/img/team/3.jpg')}}" alt=""/>
						</div>
					</div>
					<div class="col-xs-6 col-sm-3 col-md-3">
						<div class="team-wrapper-big wow bounceInUp" data-wow-delay="1s">
							<div class="team-wrapper-overlay">
								<h5>Adrian Dawn</h5>
								<p>Graeco voluptua sed ea, malorum quaeque cotidieque per eu, quo id possit dissentias.</p>
								<div class="social-icons">
									<ul class="team-social">
										<li class="social-facebook"><a href="#"><i class="fa fa-facebook"></i></a>
										</li>
										<li class="social-twitter"><a href="#"><i class="fa fa-twitter"></i></a>
										</li>
										<li class="social-google"><a href="#"><i class="fa fa-google-plus"></i></a>
										</li>
									</ul>
								</div>
							</div>
							<img src="{{url('vendor/valera/img/team/4.jpg')}}" alt=""/>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- /Section: about -->

	<!-- Section: parallax 1 -->
	<section id="parallax1" class="home-section parallax text-light" data-stellar-background-ratio="0.5">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="text-center">
						<h2 class="big-heading highlight-dark wow bounceInDown" data-wow-delay="0.2s">We start from pixel perfect pattern</h2>
					</div>
				</div>
			</div>
		</div>
	</section>
	
	<!-- Section: contact -->
	<section id="contact" class="home-section nopadd-bot color-dark bg-white text-center">
		<div class="container marginbot-50">
			<div class="row">
				<div class="col-lg-8 col-lg-offset-2">
					<div class="wow flipInY" data-wow-offset="0" data-wow-delay="0.4s">
						<div class="section-heading text-center">
							<h2 class="h-bold">Contact us</h2>
							<div class="divider-header"></div>
							<p>Lorem ipsum dolor sit amet, agam perfecto sensibus usu at duo ut iriure.</p>
						</div>
					</div>
				</div>
			</div>

		</div>

		<div class="container">

			<div class="row marginbot-80">
				<div class="col-md-8 col-md-offset-2">
					<form id="contact-form" class="wow bounceInUp" data-wow-offset="10" data-wow-delay="0.2s">
						<div class="row marginbot-20">
							<div class="col-md-6 xs-marginbot-20">
								<input type="text" class="form-control input-lg" id="name" placeholder="Enter name" required="required"/>
							</div>
							<div class="col-md-6">
								<input type="email" class="form-control input-lg" id="email" placeholder="Enter email" required="required"/>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<div class="form-group">
									<input type="text" class="form-control input-lg" id="subject" placeholder="Subject" required="required"/>
								</div>
								<div class="form-group">
									<textarea name="message" id="message" class="form-control" rows="4" cols="25" required placeholder="Message"></textarea>
								</div>
								<button type="submit" class="btn btn-skin btn-lg btn-block" id="btnContactUs">
									Send Message</button>
							


							</div>
						</div>
					</form>
				</div>
			</div>


		</div>
	</section>
	<!-- /Section: contact -->

	<!-- google map -->
	<div id="map-btn1-div">
		<a id="map-btn1" class="gmap-btn close-map-button btn-show" href="#map">
		Click here to open the map
		</a>
	


	</div>
	<a id="map-btn2" class="btn btn-skin btn-lg btn-noradius gmap-btn close-map-button btn-hide" href="#map" title="Close google map" data-toggle="tooltip" data-placement="top">
	795 Folsom Ave, Suite 600 San Francisco, CA 94107
	</a>




	<!-- google map -->
	<section id="map" class="close-map">
		<div id="google-map"></div>
	</section>
	<!-- /google map -->


	<footer>
		<div class="container">
			<div class="row">
				<div class="col-md-6 col-md-offset-3">

					<div class="text-center">
						<a href="#intro" class="totop"><i class="pe-7s-angle-up pe-3x"></i></a>

						<div class="social-widget">


							<ul class="team-social">
								<li class="social-facebook"><a href="#"><i class="fa fa-facebook"></i></a>
								</li>
								<li class="social-twitter"><a href="#"><i class="fa fa-twitter"></i></a>
								</li>
								<li class="social-google"><a href="#"><i class="fa fa-google-plus"></i></a>
								</li>
								<li class="social-dribbble"><a href="#"><i class="fa fa-dribbble"></i></a>
								</li>
							</ul>

						</div>
						<p>Valera, Inc. 795 Folsom Ave, Suite 600 San Francisco, CA 94107<br/> &copy;Copyright 2015 - Valera. All rights reserved. | Designed by <a href="http://bootstraptaste.com">BootstrapTaste</a>
						</p>
						<!-- 
                            All links in the footer should remain intact. 
                            Licenseing information is available at: http://bootstraptaste.com/license/
                            You can buy this theme without footer links online at: http://bootstraptaste.com/buy/?theme=Valera
                        -->
					</div>
				</div>
			</div>
		</div>
	</footer>

	<!-- Core JavaScript Files -->
	{!!Html::script('vendor/valera/js/jquery.min.js')!!} {!!Html::script('vendor/valera/js/bootstrap.min.js')!!} {!!Html::script('vendor/valera/js/jquery.sticky.js')!!} {!!Html::script('vendor/valera/js/slippry.min.js')!!} {!!Html::script('vendor/valera/js/jquery.flexslider-min.js')!!} {!!Html::script('vendor/valera/js/morphext.min.js')!!} {!!Html::script('vendor/valera/js/gmap.js')!!} {!!Html::script('vendor/valera/js/jquery.mb.YTPlayer.js')!!} {!!Html::script('vendor/valera/js/jquery.easing.min.js')!!} {!!Html::script('vendor/valera/js/jquery.scrollTo.js')!!} {!!Html::script('vendor/valera/js/jquery.appear.js')!!} {!!Html::script('vendor/valera/js/stellar.js')!!} {!!Html::script('vendor/valera/js/wow.min.js')!!} {!!Html::script('vendor/valera/js/owl.carousel.min.js')!!} {!!Html::script('vendor/valera/js/nivo-lightbox.min.js')!!} {!!Html::script('vendor/valera/js/jquery.nicescroll.min.js')!!} {!!Html::script('vendor/valera/js/custom.js')!!}
	<script src="http://maps.google.com/maps/api/js?sensor=false"></script>
</body>

</html>